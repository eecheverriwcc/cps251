# Git README #

This README would normally document whatever steps are necessary to get your application up and running.

### Git install 

[Git Download](https://git-scm.com/downloads)

###  How do I get set up?  ###
Get started the easy way

* create a Bitbucket account 

	[Bitbucket](https://bitbucket.org/)

* Download SourceTree

	[SourceTree](https://www.sourcetreeapp.com/)

* Download bitbucket private key from DropBox

	[Atlassian Documentation, Creating SSH Keys](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html)
	
* add the private key to your config file under :

```
	cd ~/.ssh
	
	vim config 

	Host bitbucket.org
 	HostName bitbucket.org
 	IdentityFile ~/.ssh/id_rsa_bitbucket

```

* Verify git credentials are working properly ( **you will need the password for the private key** )

	At the command line, type the following :

```
   ssh -vT git@bitbucket.org

```

If everything is working, here is the output :

```
OpenSSH_7.3p1, LibreSSL 2.4.1
debug1: Reading configuration data /Users/eecheverri/.ssh/config
debug1: /Users/eecheverri/.ssh/config line 1: Applying options for bitbucket.org
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 20: Applying options for *
debug1: /etc/ssh/ssh_config line 102: Applying options for *
debug1: Connecting to bitbucket.org [104.192.143.1] port 22.
debug1: Connection established.
debug1: identity file /Users/eecheverri/.ssh/id_rsa_bitbucket type 1
debug1: key_load_public: No such file or directory
debug1: identity file /Users/eecheverri/.ssh/id_rsa_bitbucket-cert type -1
debug1: Enabling compatibility mode for protocol 2.0
debug1: Local version string SSH-2.0-OpenSSH_7.3
debug1: Remote protocol version 2.0, remote software version conker_1.0.288-e9ef8ea app-134
debug1: no match: conker_1.0.288-e9ef8ea app-134
debug1: Authenticating to bitbucket.org:22 as 'git'
debug1: SSH2_MSG_KEXINIT sent
debug1: SSH2_MSG_KEXINIT received
debug1: kex: algorithm: curve25519-sha256@libssh.org
debug1: kex: host key algorithm: ssh-rsa
debug1: kex: server->client cipher: aes128-ctr MAC: hmac-sha2-256 compression: none
debug1: kex: client->server cipher: aes128-ctr MAC: hmac-sha2-256 compression: none
debug1: expecting SSH2_MSG_KEX_ECDH_REPLY
debug1: Server host key: ssh-rsa SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A
debug1: Host 'bitbucket.org' is known and matches the RSA host key.
debug1: Found key in /Users/eecheverri/.ssh/known_hosts:4
debug1: rekey after 4294967296 blocks
debug1: SSH2_MSG_NEWKEYS sent
debug1: expecting SSH2_MSG_NEWKEYS
debug1: rekey after 4294967296 blocks
debug1: SSH2_MSG_NEWKEYS received
debug1: SSH2_MSG_SERVICE_ACCEPT received
debug1: Authentications that can continue: publickey
debug1: Next authentication method: publickey
debug1: Offering RSA public key: /Users/eecheverri/.ssh/id_rsa_bitbucket
debug1: Server accepts key: pkalg ssh-rsa blen 279
Enter passphrase for key '/Users/eecheverri/.ssh/id_rsa_bitbucket': 
debug1: Authentication succeeded (publickey).
Authenticated to bitbucket.org ([104.192.143.1]:22).
debug1: channel 0: new [client-session]
debug1: Entering interactive session.
debug1: pledge: network
debug1: Sending environment.
debug1: Sending env LANG = en_US.UTF-8
authenticated via a deploy key.


You can use git or hg to connect to Bitbucket. Shell access is disabled.

This deploy key has read access to the following repositories:

ericecheverri/ecr_test: id_rsa_bitbucket.pub -- eric.echeverri@altarum.org
hci3/ecr-integration: id_rsa_bitbucket.pub -- eric.echeverri@altarum.org
debug1: client_input_channel_req: channel 0 rtype exit-status reply 0
debug1: channel 0: free: client-session, nchannels 1
Transferred: sent 2744, received 1952 bytes, in 0.2 seconds
Bytes per second: sent 12371.8, received 8800.9
debug1: Exit status 0

```

* Login into bitbucket and clone the repository (**USE HTTPS**, do not use **SSH** ). 



```
        git clone https://eecheverriwcc@bitbucket.org/eecheverriwcc/cpi253.git

	cd cpi253

```

![https clonning](http://www.droidaddiction.com/gitclone.png)


## Working With Git


* Establish your Git Identity

	The first thing you should do when you install Git is to set your user name and email address. This is important because every Git commit uses this information, and it�s immutably baked into the commits you start creating:


```
	$ git config --global user.name "Eric Echeverri"
	$ git config --global user.email eecheverri@wccnet.edu

```

[Git identity](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

* Establish the gitignore file

	**gitignore - Specifies intentionally untracked files to ignore**
	
	[Git ignore](https://git-scm.com/docs/gitignore)
	
```
cd ~/    go to home directory

vim .gitignore	
		
```

Add the contents of the gitignore file in Dropbox

```
# Covers JetBrains IDEs: IntelliJ, RubyMine, PhpStorm, AppCode, PyCharm, CLion, Android Studio and Webstorm
# Reference: https://intellij-support.jetbrains.com/hc/en-us/articles/206544839

# User-specific stuff:
.idea/**/workspace.xml
.idea/**/tasks.xml
.idea/dictionaries

# Sensitive or high-churn files:
.idea/**/dataSources/
.idea/**/dataSources.ids
.idea/**/dataSources.xml
.idea/**/dataSources.local.xml
.idea/**/sqlDataSources.xml
.idea/**/dynamic.xml
.idea/**/uiDesigner.xml

# Gradle:
.idea/**/gradle.xml
.idea/**/libraries

# Mongo Explorer plugin:
.idea/**/mongoSettings.xml

## File-based project format:
*.iws

## Plugin-specific files:

# IntelliJ
/out/

# mpeltonen/sbt-idea plugin
.idea_modules/

# JIRA plugin
atlassian-ide-plugin.xml

# Crashlytics plugin (for Android Studio and IntelliJ)
com_crashlytics_export_strings.xml
crashlytics.properties
crashlytics-build.properties
fabric.properties

.metadata
bin/
tmp/
*.tmp
*.bak
*.swp
*~.nib
local.properties
.settings/
.loadpath
.recommenders

# Eclipse Core
.project

# External tool builders
.externalToolBuilders/

# Locally stored "Eclipse launch configurations"
*.launch

# PyDev specific (Python IDE for Eclipse)
*.pydevproject

# CDT-specific (C/C++ Development Tooling)
.cproject

# JDT-specific (Eclipse Java Development Tools)
.classpath

# Java annotation processor (APT)
.factorypath

# PDT-specific (PHP Development Tools)
.buildpath

# sbteclipse plugin
.target

# Tern plugin
.tern-project

# TeXlipse plugin
.texlipse

# STS (Spring Tool Suite)
.springBeans

# Code Recommenders
.recommenders/

# Scala IDE specific (Scala & Java development for Eclipse)
.cache-main
.scala_dependencies
.worksheet
```

## Git Basic commands


```
git clone
git branch 
git checkout
git checkout branch 
git checkout master  
git push 
git pull
git branch 
git fetch 
git commit -m "Readme.md file"
git reset --hard HEAD
git stash 
git stash list 
git stash pop
git merge
git rebase
git status

```

## Android Studio




![https clonning](http://www.droidaddiction.com/git1.png)

![https clonning](http://www.droidaddiction.com/git2.png)

![https clonning](http://www.droidaddiction.com/git3.png)



##  Cloning Learning Units

* Go to to the directory where you are going to clone the repository

```
cd /Users/eecheverri/Downloads/LearningUnitsAndroidStudioA7Samples/StateChange

```

* Issue the following command to initialize the repo **after** you have installed git in your computer:

```
git int


```

* add origin 


```
git remote add origin https://eecheverriwcc@bitbucket.org/eecheverriwcc/statechange.git

```


* Now you can do some work. To see if you have done some modifications:

```

> git status

On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore
	.idea/
	app/
	build.gradle
	gradle.properties
	gradle/
	gradlew
	gradlew.bat
	settings.gradle


```

* Add your changes

```

git add .

```

* See what you added 

```
git status
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   .gitignore
	new file:   .idea/compiler.xml
	new file:   .idea/copyright/profiles_settings.xml
	new file:   .idea/gradle.xml
	new file:   .idea/runConfigurations.xml
	new file:   app/.gitignore
	new file:   app/build.gradle
	new file:   app/proguard-rules.pro
	new file:   app/src/androidTest/java/com/ebookfrenzy/statechange/ExampleInstrumentedTest.java
	new file:   app/src/main/AndroidManifest.xml
	new file:   app/src/main/java/com/ebookfrenzy/statechange/StateChangeActivity.java
	new file:   app/src/main/java/com/ebookfrenzy/statechange/StateChangeActivityLogI.java
	new file:   app/src/main/res/layout/activity_state_change.xml
	new file:   app/src/main/res/layout/content_state_change.xml
	new file:   app/src/main/res/menu/menu_state_change.xml
	new file:   app/src/main/res/mipmap-hdpi/ic_launcher.png
	new file:   app/src/main/res/mipmap-mdpi/ic_launcher.png
	new file:   app/src/main/res/mipmap-xhdpi/ic_launcher.png
	new file:   app/src/main/res/mipmap-xxhdpi/ic_launcher.png
	new file:   app/src/main/res/mipmap-xxxhdpi/ic_launcher.png
	new file:   app/src/main/res/values-v21/styles.xml
	new file:   app/src/main/res/values-w820dp/dimens.xml
	new file:   app/src/main/res/values/colors.xml
	new file:   app/src/main/res/values/dimens.xml
	new file:   app/src/main/res/values/strings.xml
	new file:   app/src/main/res/values/styles.xml
	new file:   app/src/test/java/com/ebookfrenzy/statechange/ExampleUnitTest.java
	new file:   build.gradle
	new file:   gradle.properties
	new file:   gradle/wrapper/gradle-wrapper.jar
	new file:   gradle/wrapper/gradle-wrapper.properties
	new file:   gradlew
	new file:   gradlew.bat
	new file:   settings.gradle


```

* Commit changes:

```

>git commit -m "StateChange Using Log info  and Toast classes"


[master (root-commit) 7805970] StateChange Using Log info  and Toast classes
 34 files changed, 794 insertions(+)
 create mode 100755 .gitignore
 create mode 100644 .idea/compiler.xml
 create mode 100644 .idea/copyright/profiles_settings.xml
 create mode 100644 .idea/gradle.xml
 create mode 100644 .idea/runConfigurations.xml
 create mode 100755 app/.gitignore
 create mode 100755 app/build.gradle
 create mode 100755 app/proguard-rules.pro
 create mode 100755 app/src/androidTest/java/com/ebookfrenzy/statechange/ExampleInstrumentedTest.java
 create mode 100755 app/src/main/AndroidManifest.xml
 create mode 100755 app/src/main/java/com/ebookfrenzy/statechange/StateChangeActivity.java
 create mode 100644 app/src/main/java/com/ebookfrenzy/statechange/StateChangeActivityLogI.java
 create mode 100755 app/src/main/res/layout/activity_state_change.xml
 create mode 100755 app/src/main/res/layout/content_state_change.xml
 create mode 100755 app/src/main/res/menu/menu_state_change.xml
 create mode 100755 app/src/main/res/mipmap-hdpi/ic_launcher.png
 create mode 100755 app/src/main/res/mipmap-mdpi/ic_launcher.png
 create mode 100755 app/src/main/res/mipmap-xhdpi/ic_launcher.png
 create mode 100755 app/src/main/res/mipmap-xxhdpi/ic_launcher.png
 create mode 100755 app/src/main/res/mipmap-xxxhdpi/ic_launcher.png
 create mode 100755 app/src/main/res/values-v21/styles.xml
 create mode 100755 app/src/main/res/values-w820dp/dimens.xml
 create mode 100755 app/src/main/res/values/colors.xml
 create mode 100755 app/src/main/res/values/dimens.xml
 create mode 100755 app/src/main/res/values/strings.xml
 create mode 100755 app/src/main/res/values/styles.xml
 create mode 100755 app/src/test/java/com/ebookfrenzy/statechange/ExampleUnitTest.java
 create mode 100755 build.gradle
 create mode 100755 gradle.properties
 create mode 100755 gradle/wrapper/gradle-wrapper.jar
 create mode 100755 gradle/wrapper/gradle-wrapper.properties
 create mode 100755 gradlew
 create mode 100755 gradlew.bat
 create mode 100755 settings.gradle


```

* Push changes to origin

```
 git push origin master
Counting objects: 68, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (44/44), done.
Writing objects: 100% (68/68), 87.64 KiB | 0 bytes/s, done.
Total 68 (delta 4), reused 0 (delta 0)
To https://bitbucket.org/eecheverriwcc/statechange.git
 * [new branch]      master -> master


```


